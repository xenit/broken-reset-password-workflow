package eu.xenit.troubleshooting.reproduction;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.github.dynamicextensionsalfresco.annotations.RunAs;
import com.github.dynamicextensionsalfresco.annotations.RunAsSystem;
import com.github.dynamicextensionsalfresco.webscripts.annotations.Authentication;
import com.github.dynamicextensionsalfresco.webscripts.annotations.AuthenticationType;
import com.github.dynamicextensionsalfresco.webscripts.annotations.HttpMethod;
import com.github.dynamicextensionsalfresco.webscripts.annotations.RequestParam;
import com.github.dynamicextensionsalfresco.webscripts.annotations.Uri;
import com.github.dynamicextensionsalfresco.webscripts.annotations.WebScript;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.repo.workflow.WorkflowModel;
import org.alfresco.repo.workflow.WorkflowModelResetPassword;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.cmr.workflow.WorkflowDefinition;
import org.alfresco.service.cmr.workflow.WorkflowService;
import org.alfresco.service.namespace.QName;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ResponseBody;

@Component
@Authentication(AuthenticationType.NONE)
@WebScript(families = "Bugs Reproduction", baseUri = "/bugs/reproduction")
public class TestWebScript {
    private static final QName WF_PROP_USERNAME_ACTIVITI = QName.createQName(WorkflowModelResetPassword.WF_PROP_USERNAME_ACTIVITI);
    private static final QName WF_PROP_USER_EMAIL_ACTIVITI = QName.createQName(WorkflowModelResetPassword.WF_PROP_USER_EMAIL_ACTIVITI);
    private static final QName WF_PROP_CLIENT_NAME_ACTIVITI = QName.createQName(WorkflowModelResetPassword.WF_PROP_CLIENT_NAME_ACTIVITI);

    private final WorkflowService workflowService;
    private final PersonService personService;
    private final RetryingTransactionHelper transactionHelper;

    public TestWebScript(WorkflowService workflowService, PersonService personService, RetryingTransactionHelper transactionHelper) {
        this.workflowService = workflowService;
        this.personService = personService;
        this.transactionHelper = transactionHelper;
    }

    @ResponseBody
    @Uri(value = "/brokenResetPasswordWorkflow", method = HttpMethod.POST)
    public ResponseEntity<String> brokenResetPasswordWorkflow(@RequestParam final String email) {
        return AuthenticationUtil.runAsSystem(() ->
            transactionHelper.doInTransaction(() -> {
                startWorkFlow(email);
                return new ResponseEntity<>("Success!", HttpStatus.OK);
            }, false, true)
        );
    }

    private void startWorkFlow(final String email) {
        final NodeRef user = getUserByUserName(email);
        WorkflowDefinition workflowDefinition = workflowService.getDefinitionByName("activiti$resetPassword");
        if (workflowDefinition == null) {
            throw new RuntimeException("Workflow not deployed");
        }
        Map<QName, Serializable> param = new HashMap<>();
        param.put(WorkflowModel.ASSOC_ASSIGNEE, user);
        param.put(WorkflowModel.PROP_DESCRIPTION, "Request to change password");
        param.put(ContentModel.PROP_OWNER, user);

        param.put(WF_PROP_USERNAME_ACTIVITI,email);
        param.put(WF_PROP_USER_EMAIL_ACTIVITI, email);
        param.put(WF_PROP_CLIENT_NAME_ACTIVITI, "share");

        workflowService.startWorkflow(workflowDefinition.getId(), param);
    }

    private NodeRef getUserByUserName(final String userName) {
        NodeRef user = personService.getPersonOrNull(userName);
        if (user == null) {
            throw new WebScriptException(404, "User with username " + userName + " not found");
        }
        return user;
    }
}
